import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from './header/header.component';

@Component({
    selector: 'my-app',
    styleUrls: ['app/app.component.css'],
    templateUrl: 'app/app.component.html'
})

export class AppComponent implements OnInit {
    constructor () { }
    ngOnInit() { }
}
