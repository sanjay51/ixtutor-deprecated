import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './../authentication.service';

@Component({
    selector: 'my-header',
    styleUrls: ['app/header/header.component.css'],
    templateUrl: 'app/header/header.component.html'
})

export class HeaderComponent implements OnInit {
    title = 'ixTutor';

    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    ngOnInit() {

    }

    gotoLogin() {
        let link = ["/login"];
        this.router.navigate(link);
    }

    logout() {
        this.authenticationService.logout();
    }

}
